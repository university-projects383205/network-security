#!/bin/bash

iptables -t nat -A PREROUTING -i eth0 -s 192.168.1.4 -p tcp --dport 80 -j REDIRECT --to-port 8080
