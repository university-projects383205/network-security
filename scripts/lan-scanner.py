#!/usr/bin/python3.8

from scapy.layers.l2 import ARP, Ether
from scapy.sendrecv import srp
import argparse
import re
import socket
import ipaddress
import os
import sys
import tqdm


def scan(args):
    hosts = []

    if args.subnet:
        if args.verbose:
            print(f"[*] Subnet: {args.subnet}")

        try:
            subnet = ipaddress.IPv4Network(args.subnet)

            for address in tqdm.tqdm(list(subnet.hosts())):
                pkt = Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=str(address))
                ans, not_ans = srp(pkt, iface=args.interface, timeout=0.3, verbose=0)

                if ans:
                    hosts.append(str(address))
                    if args.verbose:
                        print(f"[*] Found host at {address}")

        except ipaddress.AddressValueError:
            sys.exit("The specified subnet is not valid!")
        except ValueError as ex:
            sys.exit(ex)
    else:
        sys.exit("Cannot automatically detect the subnet, try to manually specify it!")

    return hosts


def get_default_subnet():
    match = re.search(r"\d+\.\d+\.\d+", socket.gethostbyname(socket.gethostname()))

    if match:
        return f"{match.group()}.0/24"
    else:
        return None


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--subnet", help="subnet to scan (CIDR notation)", type=str, default=get_default_subnet())
    parser.add_argument("-i", "--interface", help="set network interface", type=str, default="eno1")
    parser.add_argument("-v", "--verbose", help="enable verbose output", action="store_true")
    args = parser.parse_args()
    hosts = []

    try:
        hosts = scan(args)
    except KeyboardInterrupt:
        print()
        sys.exit()
    finally:
        print(f"[*] Number of hosts found: {len(hosts)}")
        print(f"[*] Hosts found: {hosts}")


if __name__ == "__main__":
    if sys.version_info >= (3, 8):  # Useless check
      if os.geteuid() == 0:
        main()
      else:
        sys.exit("This script must be run as root!")
    else:
        sys.exit("This script must be run with Python 3.8 or higher!")
