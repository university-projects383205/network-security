#!/usr/bin/python3.8

from scapy.layers.inet import IP, TCP
from scapy.sendrecv import send, sr1
from scapy.volatile import RandNum
import argparse
import tqdm


def scan(args):
    open_ports = []

    for port_number in tqdm.tqdm(range(1, 1024)):
        src_port = RandNum(1025, 65535)
        if args.syn:
            probe_pkt = IP(dst=args.target_ip) / TCP(sport=src_port, dport=port_number, flags="S")
        else:
            probe_pkt = IP(dst=args.target_ip) / TCP(sport=src_port, dport=port_number, flags="F")

        reply = sr1(probe_pkt, verbose=0, timeout=1)

        if args.syn:
            if reply and "R" not in reply["TCP"].flags:
                open_ports.append(port_number)
                send(IP(dst=args.target_ip) / TCP(sport=src_port, dport=port_number, flags="R"), verbose=0)
        else:
            if reply is None:
                open_ports.append(port_number)

    return open_ports


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("target_ip", help="set target IP address", type=str)
    parser.add_argument("--syn", help="use SYN scanning instead of FIN scanning", action="store_true")
    args = parser.parse_args()
    open_ports = []

    try:
        open_ports = scan(args)
    except KeyboardInterrupt:
        print()
    finally:
        print(open_ports)


if __name__ == "__main__":
    main()
